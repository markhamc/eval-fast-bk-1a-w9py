FORMAT: 1A

# Fort Speakeasy API
Code assessment.

This documentation was auto-generated from the Swagger/OpenAPI JSON file.

# Group Bars

## Api Bars By CityId [/api/Bars/{cityId}]

+ Parameters
    + cityId (string, required)


### ApiBarsByCityIdGET [GET]

+ Response 500 (application/json)

        Server Error

    + Attributes (string)




## Api Bars By CityId And BarId [/api/Bars/{cityId}/{barId}]

+ Parameters
    + cityId (string, required)

    + barId (string, required)


### ApiBarsByCityIdAndBarIdPUT [PUT]

+ Request (<<Add Header Value>>)

    + Attributes (AddBarRequest)



+ Response 500 (application/json)

        Server Error

    + Attributes (string)



### ApiBarsByCityIdAndBarIdDELETE [DELETE]

+ Response 500 (application/json)

        Server Error

    + Attributes (string)





# Group Cities

## Api Cities [/api/Cities]

### ApiCitiesGET [GET]

+ Response 500 (application/json)

        Server Error

    + Attributes (string)




## Api Cities By Id [/api/Cities/{id}]

+ Parameters
    + cityId (string, required)

    + id (string, required)


### ApiCitiesByIdPUT [PUT]

+ Request (<<Add Header Value>>)

    + Attributes (AddCityRequest)



+ Response 500 (application/json)

        Server Error

    + Attributes (string)





# Group SharedBars

## Api SharedBars [/api/SharedBars]

### ApiSharedBarsGET [GET]

+ Response 500 (application/json)

        Server Error

    + Attributes (string)





# Group User

## Api User Login [/api/User/login]

### ApiUserLoginPOST [POST]

+ Request (<<Add Header Value>>)

    + Attributes (UserLoginRequest)



+ Response 500 (application/json)

        Server Error

    + Attributes (string)



+ Response 404 (application/json)

        Not Found

    + Attributes (string)




## Api User Register [/api/User/register]

### ApiUserRegisterPOST [POST]

+ Request (<<Add Header Value>>)

    + Attributes (UserRegistrationRequest)



+ Response 500 (application/json)

        Server Error

    + Attributes (string)



+ Response 409 (application/json)

        Conflict

    + Attributes (string)





# Data Structures

## AddBarRequest (object)


### Properties
+ `barName` (string, optional, nullable) 
+ `favoriteDrink` (string, optional, nullable) 


## AddCityRequest (object)


### Properties
+ `cityName` (string, optional, nullable) 
+ `countryName` (string, optional, nullable) 


## UserLoginRequest (object)


### Properties
+ `email` (string, optional, nullable) 
+ `password` (string, optional, nullable) 


## UserRegistrationRequest (object)


### Properties
+ `name` (string, optional, nullable) 
+ `email` (string, optional, nullable) 
+ `password` (string, optional, nullable) 

