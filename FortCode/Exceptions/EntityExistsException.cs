using System;

namespace FortCode.Exceptions
{
    public class EntityExistsException : Exception
    {
        public EntityExistsException(string? message) : base(message) { }
    }
}