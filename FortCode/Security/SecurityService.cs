using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using FortCode.Exceptions;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace FortCode.Security
{
    public class SecurityService
    {
        private readonly IConfiguration configuration;

        public SecurityService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public static SymmetricSecurityKey GetSigningKey(IConfiguration config)
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.GetValue<string>("Jwt:Key")));
        }

        // This is a simple proof-of-concept and several shortcuts are taken.
        // In practice a 3rd party provider would be used to provided authentication.
        public JwtSecurityToken CreateToken(Guid userId, string email)
        {
            var claims = new[]{
                new Claim(JwtRegisteredClaimNames.Sub, userId.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                new Claim("Email", email)
            };
            var key = GetSigningKey(configuration);
            var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var issuer = configuration.GetValue<string>("Jwt:Issuer");
            var audience = configuration.GetValue<string>("Jwt:Audience");
            return new JwtSecurityToken(issuer, audience, claims, DateTime.UtcNow, DateTime.UtcNow.AddHours(1), signingCredentials);
        }

        public Guid GetUserId(ClaimsPrincipal user)
        {
            var claim = user.FindFirst(ClaimTypes.NameIdentifier);
            if (claim?.Value == null)
            {
                throw new NotFoundException("User ID not found in claim");
            }
            return Guid.Parse(claim.Value);
        }
    }
}