using System;

namespace FortCode.Models
{
    public class Bar
    {
        public Guid BarId { get; set; }
        public string BarName { get; set; }
        public string FavoriteDrink { get; set; }

        public Guid CityId { get; set; }
        public City City { get; set; }
    }
}