using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FortCode.Models
{
    public partial class SpeakeasyDbContext : DbContext
    {
        public SpeakeasyDbContext(DbContextOptions<SpeakeasyDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Bar> Bars { get; set; }
        public virtual DbSet<SharedBarCount> SharedBarCounts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();
            
            modelBuilder.Entity<SharedBarCount>(
                eb =>
                {
                    eb.HasNoKey();
                    eb.ToView("View_SharedBarCounts");
                }
            );
        }
    }
}