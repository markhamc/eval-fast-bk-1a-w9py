using System;
using System.Collections.Generic;

namespace FortCode.Models
{
    public class City
    {
        public Guid CityId { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }
        public List<Bar> Bars { get; set; }
    }
}