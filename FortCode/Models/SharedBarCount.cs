namespace FortCode.Models
{
    public class SharedBarCount
    {
        public string CityName { get; set; }
        public string BarName { get; set; }
        public int ShareCount { get; set; }
    }
}