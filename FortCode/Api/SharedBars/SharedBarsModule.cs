using Microsoft.Extensions.DependencyInjection;

namespace FortCode.Api.SharedBars
{
    public static class SharedBarsModule
    {
        public static IServiceCollection AddSharedBarsModule(this IServiceCollection services)
        {
            services.AddScoped<SharedBarsService>();
            return services;
        }
    }
}