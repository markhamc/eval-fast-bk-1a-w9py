using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Api.SharedBars
{
    public class SharedBarsService
    {
        private readonly SpeakeasyDbContext dbContext;

        public SharedBarsService(SpeakeasyDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        internal async Task<List<SharedBarResponse>> GetSharedBars(Guid userId)
        {
            var query = from b in dbContext.Set<Bar>()
                        join c in dbContext.Set<City>()
                            on b.CityId equals c.CityId
                        join sb in dbContext.Set<SharedBarCount>()
                            on new { b.BarName, c.CityName }
                            equals new { sb.BarName, sb.CityName }
                        where c.UserId == userId
                        select new { sb.BarName, sb.CityName, sb.ShareCount };
            return await query
                .AsNoTracking()
                .Select(x => new SharedBarResponse(){
                    BarName = x.BarName,
                    CityName = x.CityName,
                    ShareCount = x.ShareCount
                })
                .ToListAsync();
        }
    }
}