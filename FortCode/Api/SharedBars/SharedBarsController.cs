using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Api.SharedBars
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
    public class SharedBarsController : ControllerBase
    {
        private readonly SharedBarsService sharedBarsService;
        private readonly SecurityService securityService;

        public SharedBarsController(SharedBarsService sharedBarsService, SecurityService securityService)
        {
            this.sharedBarsService = sharedBarsService;
            this.securityService = securityService;
        }

        [HttpGet]
        public async Task<ActionResult<List<SharedBarResponse>>> GetSharedBars()
        {
            var userId = securityService.GetUserId(HttpContext.User);
            var bars = await sharedBarsService.GetSharedBars(userId);
            return Ok(bars);
        }
    }
}