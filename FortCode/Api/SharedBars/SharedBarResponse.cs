using System;

namespace FortCode.Api.SharedBars
{
    public class SharedBarResponse
    {
        public string BarName { get; set; }
        public string CityName { get; set; }
        public int ShareCount { get; set; }
    }
}