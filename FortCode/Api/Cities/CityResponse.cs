using System;

namespace FortCode.Api.Cities
{
    public class CityResponse
    {
        public Guid CityId { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
    }
}