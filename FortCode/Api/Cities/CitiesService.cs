using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Api.Cities
{
    public class CitiesService
    {
        private readonly SpeakeasyDbContext dbContext;

        public CitiesService(SpeakeasyDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<List<City>> GetCities(Guid userId)
        {
            return await dbContext.Cities
                .Where(c => c.UserId == userId)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<Boolean> AddCity(Guid userId, Guid cityId, AddCityRequest request)
        {
            var result = await dbContext.Cities
                .AddAsync(new City()
                {
                    CityId = cityId,
                    CityName = request.CityName,
                    CountryName = request.CountryName,
                    UserId = userId
                });
            return (await dbContext.SaveChangesAsync()) == 1;
        }
    }
}