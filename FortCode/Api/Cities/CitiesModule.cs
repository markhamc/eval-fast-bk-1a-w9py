using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FortCode.Api.Cities
{
    public static class CitiesModule
    {
        public static IServiceCollection AddCitiesModule(this IServiceCollection services)
        {
            services.AddScoped<CitiesService>();
            return services;
        }
    }
}