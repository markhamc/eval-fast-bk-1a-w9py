using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Api.Cities
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
    public class CitiesController : ControllerBase
    {
        private readonly CitiesService citiesService;
        private readonly SecurityService securityService;

        public CitiesController(CitiesService citiesService, SecurityService securityService)
        {
            this.citiesService = citiesService;
            this.securityService = securityService;
        }

        [HttpGet]
        public async Task<ActionResult<List<CityResponse>>> GetCities()
        {
            var userId = securityService.GetUserId(HttpContext.User);
            var cities = await citiesService.GetCities(userId);
            return Ok(cities.Select(c => new CityResponse()
            {
                CityId = c.CityId,
                CityName = c.CityName,
                Country = c.CountryName
            }));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> AddCity([FromRoute]Guid cityId, AddCityRequest request)
        {
            if (await citiesService.AddCity(securityService.GetUserId(HttpContext.User), cityId, request))
            {
                return Ok();
            }
            return Problem("Unable to create city");
        }
    }
}