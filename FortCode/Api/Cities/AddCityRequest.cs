using System;

namespace FortCode.Api.Cities
{
    public class AddCityRequest
    {
        public string CityName { get; set; }
        public string CountryName { get; set; }
    }
}