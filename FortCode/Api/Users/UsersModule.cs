using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FortCode.Api.Users
{
    public static class UsersModule
    {
        public static IServiceCollection AddUsersModule(this IServiceCollection services)
        {
            services.AddScoped<UserService>();
            return services;
        }
    }
}