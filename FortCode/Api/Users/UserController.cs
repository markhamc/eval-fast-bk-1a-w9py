using System.Threading.Tasks;
using FortCode.Exceptions;
using FortCode.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Api.Users
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
    public class UserController : ControllerBase
    {
        private readonly UserService userService;
        private readonly SecurityService securityService;

        public UserController(UserService userService, SecurityService securityService)
        {
            this.userService = userService;
            this.securityService = securityService;
        }

        [HttpPost("login")]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<UserResponse>> Login(UserLoginRequest request)
        {
            try
            {
                return Ok(await userService.VerifyAsync(request));
            }
            catch (NotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost("register")]
        [ProducesResponseType(typeof(string), StatusCodes.Status409Conflict)]
        public async Task<ActionResult<UserResponse>> Register(UserRegistrationRequest request)
        {
            try
            {
                return Ok(await userService.RegisterAsync(request));
            }
            catch (EntityExistsException e)
            {
                return Conflict(e.Message);
            }
        }
    }
}