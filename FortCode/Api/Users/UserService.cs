using System.Threading.Tasks;

using BC = BCrypt.Net.BCrypt;
using Microsoft.EntityFrameworkCore;

using FortCode.Models;
using FortCode.Exceptions;
using System.IdentityModel.Tokens.Jwt;
using FortCode.Security;

namespace FortCode.Api.Users
{
    public class UserService
    {
        private readonly SpeakeasyDbContext dbContext;
        private readonly SecurityService securityService;

        public UserService(SpeakeasyDbContext dbContext, SecurityService securityService)
        {
            this.dbContext = dbContext;
            this.securityService = securityService;
        }

        public async Task<UserResponse> VerifyAsync(UserLoginRequest request)
        {
            var user = await dbContext.Users.AsNoTracking().SingleOrDefaultAsync(u => u.Email == request.Email);
            if (user != null && BC.EnhancedVerify(request.Password, user.PasswordHash))
            {
                return new UserResponse
                {
                    UserId = user.UserId,
                    Email = user.Email,
                    Name = user.Name,
                    Token = new JwtSecurityTokenHandler().WriteToken(securityService.CreateToken(user.UserId, user.Email))
                };
            }
            throw new NotFoundException($"User not found with email '{request.Email}'");
        }

        internal async Task<UserResponse> RegisterAsync(UserRegistrationRequest request)
        {
            var existing = await dbContext.Users.SingleOrDefaultAsync(u => u.Email == request.Email);
            if (existing != null)
            {
                throw new EntityExistsException($"User already exists with email '{request.Email}'");
            }
            var user = new User
            {
                Email = request.Email,
                Name = request.Name,
                PasswordHash = BC.EnhancedHashPassword(request.Password)
            };
            var result = await dbContext.Users.AddAsync(user);
            await dbContext.SaveChangesAsync();
            return new UserResponse
            {
                UserId = result.Entity.UserId,
                Name = request.Name,
                Email = request.Email
            };
        }
    }
}