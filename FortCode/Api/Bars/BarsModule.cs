using Microsoft.Extensions.DependencyInjection;

namespace FortCode.Api.Bars
{
    public static class BarsModule
    {
        public static IServiceCollection AddBarsModule(this IServiceCollection services)
        {
            services.AddScoped<BarsService>();
            return services;
        }
    }
}