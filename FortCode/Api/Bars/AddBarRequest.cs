using System;

namespace FortCode.Api.Bars
{
    public class AddBarRequest
    {
        public string BarName { get; set; }
        public string FavoriteDrink { get; set; }
    }
}