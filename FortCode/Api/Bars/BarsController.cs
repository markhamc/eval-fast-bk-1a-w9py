using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FortCode.Api.Bars
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
    public class BarsController : ControllerBase
    {
        private readonly BarsService BarsService;
        private readonly SecurityService securityService;

        public BarsController(BarsService BarsService, SecurityService securityService)
        {
            this.BarsService = BarsService;
            this.securityService = securityService;
        }

        [HttpGet("{cityId}")]
        public async Task<ActionResult<List<BarResponse>>> GetBars([FromRoute]Guid cityId)
        {
            var userId = securityService.GetUserId(HttpContext.User);
            var Bars = await BarsService.GetBars(userId, cityId);
            return Ok(Bars.Select(c => new BarResponse()
            {
                BarId = c.BarId,
                BarName = c.BarName,
                FavoriteDrink = c.FavoriteDrink
            }));
        }

        [HttpPut("{cityId}/{barId}")]
        public async Task<IActionResult> AddBar([FromRoute]Guid cityId, [FromRoute]Guid barId, AddBarRequest request)
        {
            // TODO refactor to use a parameter object instead of passing series of ids
            if (await BarsService.AddBar(securityService.GetUserId(HttpContext.User), cityId, barId, request))
            {
                return Ok();
            }
            return Problem("Unable to create Bar");
        }

        [HttpDelete("{cityId}/{barId}")]
        public async Task<IActionResult> DeleteBar([FromRoute]Guid cityId, [FromRoute]Guid barId)
        {
            // TODO refactor to use a parameter object instead of passing series of ids
            if (await BarsService.DeleteBar(securityService.GetUserId(HttpContext.User), cityId, barId))
            {
                return Ok();
            }
            return Problem("Unable to delete Bar");
        }
    }
}