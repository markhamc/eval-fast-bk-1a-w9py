using System;

namespace FortCode.Api.Bars
{
    public class BarResponse
    {
        public Guid BarId { get; set; }
        public string BarName { get; set; }
        public string FavoriteDrink { get; set; }
    }
}