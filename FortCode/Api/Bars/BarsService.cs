using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Api.Bars
{
    public class BarsService
    {
        private readonly SpeakeasyDbContext dbContext;

        public BarsService(SpeakeasyDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        internal async Task<List<Bar>> GetBars(Guid userId, Guid cityId)
        {
            return (await dbContext.Cities
                .AsNoTracking()
                .Include(c => c.Bars)
                .SingleAsync(c => c.UserId == userId && c.CityId == cityId))
                .Bars;
        }

        internal async Task<bool> AddBar(Guid userId, Guid cityId, Guid barId, AddBarRequest request)
        {
            var city = await dbContext.Cities
                .AsNoTracking()
                .SingleAsync(c => c.CityId == cityId && c.UserId == userId);
            if (city == null)
            {
                throw new UnauthorizedAccessException();
            }
            var result = await dbContext.Bars
                .AddAsync(new Bar()
                {
                    BarId = barId,
                    BarName = request.BarName,
                    FavoriteDrink = request.FavoriteDrink,
                    CityId = cityId
                });
            return (await dbContext.SaveChangesAsync()) == 1;
        }

        internal async Task<bool> DeleteBar(Guid userId, Guid cityId, Guid barId)
        {
            var city = await dbContext.Cities
                .AsNoTracking()
                .SingleAsync(c => c.CityId == cityId && c.UserId == userId);
            if (city == null)
            {
                throw new UnauthorizedAccessException();
            }
            var bar = await dbContext.Bars
                .SingleAsync(b => b.BarId == barId && b.CityId == cityId);
            dbContext.Remove(bar);
            return (await dbContext.SaveChangesAsync()) == 1;
        }
    }
}