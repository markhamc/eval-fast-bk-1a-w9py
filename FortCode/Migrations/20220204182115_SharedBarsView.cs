﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FortCode.Migrations
{
    public partial class SharedBarsView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                CREATE VIEW View_SharedBarCounts AS
                SELECT b.BarName, c.CityName, count(*) as ShareCount FROM dbo.Bars as b
                        JOIN dbo.Cities as c
                        ON b.CityId = c.CityId
                        GROUP BY c.CityName, b.BarName;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP VIEW View_SharedBarCounts");
        }
    }
}
