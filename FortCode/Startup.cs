using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

using FortCode.Models;
using FortCode.Api.Users;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using FortCode.Api.Cities;
using FortCode.Security;
using FortCode.Api.Bars;
using FortCode.Api.SharedBars;

namespace FortCode
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc();

            services
                .AddSingleton<SecurityService>();

            // Add vertical slices
            services
                .AddUsersModule()
                .AddCitiesModule()
                .AddBarsModule()
                .AddSharedBarsModule();

            services
                .AddControllers()
                .AddNewtonsoftJson();

            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false; // This is a security risk and is only for this simple POC
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidAudience = Configuration.GetValue<string>("Jwt:Audience"),
                        ValidIssuer = Configuration.GetValue<string>("Jwt:Issuer"),
                        IssuerSigningKey = SecurityService.GetSigningKey(Configuration)
                    };
                });

            services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Title = "Fort Speakeasy API",
                        Version = "v1",
                        Description = "Code assessment",
                        Contact = new OpenApiContact
                        {
                            Name = "Cole Markham",
                            Email = "markhamc@gmail.com",
                            Url = new Uri("https://github.com/colemarkham")
                        }
                    });
                });

            var connection = Configuration.GetConnectionString("DbContext");
            services
                .AddDbContextPool<SpeakeasyDbContext>(options => options.UseSqlServer(connection));
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, SpeakeasyDbContext dbContext)
        {
            if (Configuration.GetValue<Boolean>("DatabaseMigrationEnabled"))
            {
                dbContext.Database.Migrate();
            }
            app
                .UseFileServer()
                .UseRouting()
                .UseAuthentication()
                .UseAuthorization()
                .UseEndpoints(endPoints => { endPoints.MapControllers(); });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // Often this is only enabled for development mode, but since I'm testing with Production mode in docker-compose it's always enabled.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Speakeasy API V1");
            });
        }
    }
}
