# Running the application

Potentially sensitive data was moved out of the docker-compose.yml file and into the .env file which is ignored from version control. To run the application, you need to create a .env file in the same directory as docker-compose.yml with the following variables defined:

- FORT_DB_PASSWORD
- FORT_JWT_KEY

# Architecture

I used a vertical slice architecture that groups code based on feature rather than using a layered or onion approach. Within each module, different approaches can be used as appropriate. For the purposes of this application, all of the slices have a similar design.

Within each module, the controller handles all of the HTTP-specific code. The service encapsulates most of the logic for the module. This separation is sufficient to isolute the two main components of each module (REST API and Database entities) without needing additional layers (such as repositories).

The API uses Guids for the entity ids with PUT endpoints that expect the id. This allows the ids to be created by the client and known ahead of time before the entity is persisted. This would aid with implementing offline functionality and scalability of the system.

# Limitations

- There are no checks to prevent a single user from adding multiple bars or cities with the same name.
    - This would also affect the results of the SharedBars query.
- The authentication system is very much a proof-of-concept. For a real system, an external identity provider would most likely be used.
- The EF queries are not well optimized.
    - When working with Bars, the corresponding City object must be fetched to ensure that the user is authorized. This could be moved to a single DB query, or the userId could be attached to the lower-level entities as well.
    - The shared bars module has a better example of how to do the request in a single query.
- I planned to write automated tests of the API services and/or unit tests at the service level, but I ran out of time.
- The swagger UI is generated and allows viewing the API, but it does not have the Bearer token authentication implemented so it is not usable for testing.
- Some of the service methods need refactoring to avoid ambiguous method signatures.
- Deleting of Cities is not currently implemented because I didn't have time to handle the case where there are bars associated with them.


# Future work

- Add middleware to handle the exception return types. This will help with some of the duplciated boilerplate code in the controllers.
- Improve documentation of the API. The current Api.md was auto-generated from the Swagger/OpenAPI doc. Better descripions should be added to the code so this documentation is more user-friendly.